﻿using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.UI.Controls;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Attached property for a <see cref="SceneView"/> that sets the viewpoint of the camera when changed
    /// </summary>
    public class SceneCameraAttachedProperty : BaseAttachedProperty<SceneCameraAttachedProperty, Camera>
    {
        public override async void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is SceneView sceneView))
                return;

            // Get the new camera
            var newCamera = (Camera)e.NewValue;

            // Set the viewpoint camera to changed value
            await sceneView.SetViewpointCameraAsync(newCamera);
        }
    }

    /// <summary>
    /// Attached property for a <see cref="SceneView"/> that changes the viewpoint based on the envelope property
    /// </summary>
    public class SceneEnvelopeProperty : BaseAttachedProperty<SceneEnvelopeProperty, Envelope>
    {
        public override async void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is SceneView sceneView))
                return;

            // Cast the args to an envelope
            var envelope = (Envelope)e.NewValue;

            // Get the field of view of the sceneview
            var fieldOfView = sceneView.FieldOfView;

            // Calculate the new camera position
            Camera newCamera = Utilities.CalculateOptimalCameraPosition(envelope, fieldOfView);

            // Set the camera position
            await sceneView.SetViewpointCameraAsync(newCamera);
        }
    }

    /// <summary>
    /// Attached property to change the viewpoint camera based on a line segment
    /// </summary>
    public class SceneLineSegmentProperty : BaseAttachedProperty<SceneLineSegmentProperty, LineSegment>
    {
        public override async void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is SceneView sceneView))
                return;

            // Cast the args to a line segment
            var lineSegment = (LineSegment)e.NewValue;

            // Get the new cameria position
            Camera newCamera = Utilities.CalculateOptimalCameraPosition(lineSegment, sceneView.FieldOfView);

            // Set the camera position
            await sceneView.SetViewpointCameraAsync(newCamera);
        }
    }

    /// <summary>
    /// Attached property to display graphics onto a SceneView
    /// </summary>
    public class SceneGraphicsOverlayProperty : BaseAttachedProperty<SceneGraphicsOverlayProperty, GraphicsOverlayCollection>
    {
        public override void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is SceneView sceneView))
                return;

            // Cast the args to graphics overlay collection
            var graphics = (GraphicsOverlayCollection)e.NewValue;

            // Add the new graphics overlap collection
            sceneView.GraphicsOverlays = graphics;
        }
    }
}
