﻿using Esri.ArcGISRuntime.Mapping;
using System;
using System.Globalization;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Converter that takes in a <see cref="MenuItemType"/> and returns a <see cref="Visibility"/>
    /// based on the given parameter being the same as the menu item type 
    /// </summary>
    public class CameraToStringConverter : BaseValueConverter<CameraToStringConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var camera = (value as Camera);
            var location = camera.Location;
            return string.Format("X: {0:F4}, Y: {1:F4}, Z: {2:F4}, Heading: {3:F4}, Pitch: {4:F4}, Roll: {5}",
                location.X, location.Y, location.Z, camera.Heading, camera.Pitch, camera.Roll);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
