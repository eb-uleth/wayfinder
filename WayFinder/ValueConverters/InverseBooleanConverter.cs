﻿using System;
using System.Globalization;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Converter that takes in a <see cref="MenuItemType"/> and returns a <see cref="Visibility"/>
    /// based on the given parameter being the same as the menu item type 
    /// </summary>
    public class InverseBooleanConverter : BaseValueConverter<InverseBooleanConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new InvalidOperationException("The target must be a boolean");

            return !(bool)value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
