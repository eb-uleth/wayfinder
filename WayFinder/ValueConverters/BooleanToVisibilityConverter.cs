﻿using System;
using System.Globalization;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Converter that takes in a <see cref="MenuItemType"/> and returns a <see cref="Visibility"/>
    /// based on the given parameter being the same as the menu item type 
    /// </summary>
    public class BooleanToVisibilityConverter : BaseValueConverter<BooleanToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Cast the value to a string
            var boolValue = (bool)value;

            return boolValue ? Visibility.Visible : Visibility.Collapsed;

        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
