﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace WayFinder
{
    /// <summary>
    /// A converter that takes in a string value and returns if it is empty or null
    /// </summary>
    public class ValueToEnabledConverter : BaseValueConverter<ValueToEnabledConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Cast value to a string
            var stringValue = (string)value;

            // If the value is null or empty, the element should not be enabled
            return !string.IsNullOrEmpty(stringValue);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
