﻿using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.UI.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        /// <summary>
        /// Public list of room types that are valid to be displayed on click
        /// </summary>
        private List<string> DisplayableRoomTypes { get; } = new List<string>
        {
            "Classroom", "Office", "Lab", "Bathroom", "Other", "Meeting"
        };

        /// <summary>
        /// Find features that intersect with the tapped location and add them to the layer selection
        /// </summary>
        /// <param name="sender">The scene view</param>
        /// <param name="e">Tapped location</param>
        private async void SceneViewTapped(object sender, GeoViewInputEventArgs e)
        {
            // If the screen is clicked/tapped while animating, stop animating
            if (MainSceneView.IsNavigating)
                MainSceneView.CancelSetViewpointOperations();

            try
            {
                // Identify features on the room layer at the tap point.
                // Use a 10-pixel tolerance around the point and return a maximum of 1 feature
                IdentifyLayerResult result = await MainSceneView.IdentifyLayerAsync(((MainWindowViewModel)DataContext).SceneViewModel.RoomsLayer,
                    e.Position, 10, false, 3);

                // Filter the resulting geoelements based on the roomtype
                GeoElement filteredResult = result.GeoElements.
                    Where(f => DisplayableRoomTypes.Contains(((Feature)f).Attributes["ROOMTYPE"]?.ToString() ?? "")).FirstOrDefault();

                // If there are any results..
                if (filteredResult != null)
                {
                    // Get the selected feature
                    var feature = (Feature)filteredResult;

                    CalloutDefinition callout = new CalloutDefinition(feature);

                    var attr = feature.Attributes;

                    // If there is a room alias
                    if (!string.IsNullOrEmpty(attr["ROOMALIAS"]?.ToString()))
                    {
                        // Set the callout main text to the alias
                        callout.Text = attr["ROOMALIAS"]?.ToString();

                        // If there is also a room number, set the detail text to the room number
                        if (!string.IsNullOrEmpty(attr["ROOMNUM"]?.ToString()))
                            callout.DetailText = string.Format("Room Number: {0}\n", attr["ROOMNUM"]?.ToString());
                    }
                    // If no alias, but a room number, display the room number in the call out main text
                    else if (!string.IsNullOrEmpty(attr["ROOMNUM"]?.ToString()))
                        callout.Text = attr["ROOMNUM"].ToString();
                    // If no room number, just show the FID
                    else
                        callout.Text = string.Format("Room {0}", attr["FID"]?.ToString());

                    // Add the room type to the callout
                    if (!string.IsNullOrEmpty(attr["ROOMTYPE"]?.ToString()))
                        callout.DetailText += "Room Type: " + attr["ROOMTYPE"]?.ToString();


                    MainSceneView.ShowCalloutForGeoElement(feature, e.Position, callout);
                }
                else
                {
                    // If not the room layer, try the building layer
                    result = await MainSceneView.IdentifyLayerAsync(((MainWindowViewModel)DataContext).SceneViewModel.BuildingsSolidLayer,
                    e.Position, 10, false, 5);

                    // If a GeoElement was identified, select it in the scene.
                    if (result.GeoElements.Any())
                    {
                        GeoElement geoElement = result.GeoElements.FirstOrDefault();
                        if (geoElement != null)
                        {
                            // Get the selected feature
                            var feature = (Feature)geoElement;

                            CalloutDefinition buildingCalloutDefinition = new CalloutDefinition(feature)
                            {
                                DetailText = "Building Acronym: " + feature.Attributes["BACRONYM"]?.ToString()
                            };

                            MainSceneView.ShowCalloutForGeoElement(feature, e.Position, buildingCalloutDefinition);
                        }
                    }
                    else
                    {
                        // dismiss callout and clear all selections
                        MainSceneView.DismissCallout();
                        MainSceneView.Scene.OperationalLayers.ToList().ForEach(layer =>
                        {
                            if (layer is FeatureLayer)
                                ((FeatureLayer)layer)?.ClearSelection();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }
    }
}
