﻿namespace WayFinder
{
    /// <summary>
    /// Enumeration of the different travel modes
    /// </summary>
    public enum TravelModes
    {
        /// <summary>
        /// Denotes the path that travels the least distance
        /// </summary>
        Shortest = 0,

        /// <summary>
        /// Denotes the path that takes the least amount of time
        /// </summary>
        Fastest = 1,
    }
}
