﻿namespace WayFinder
{
    /// <summary>
    /// Enumeration for the types of buildings in the WayFinder application
    /// </summary>
    public enum BuildingType
    {
        /// <summary>
        /// No building type
        /// </summary>
        None = 0,
        /// <summary>
        /// Start building, at the beginning of the route
        /// </summary>
        Start = 1,
        /// <summary>
        /// Destination building, at the end of the route
        /// </summary>
        Destination = 2
    }
}
