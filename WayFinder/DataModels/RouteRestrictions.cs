﻿namespace WayFinder
{
    /// <summary>
    /// Enumeration for the types of restrictions that can be placed along a route
    /// </summary>
    public enum RouteRestrictions
    {
        /// <summary>
        /// No restriction, find the best route
        /// </summary>
        None = 0,

        /// <summary>
        /// Do not take any stairs
        /// </summary>
        AvoidStairs = 1,

        /// <summary>
        /// Do not take any elevators
        /// </summary>
        AvoidElevators = 2,
    }
}
