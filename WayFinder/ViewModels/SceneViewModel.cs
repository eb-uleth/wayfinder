﻿using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks.NetworkAnalysis;
using Esri.ArcGISRuntime.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace WayFinder
{
    /// <summary>
    /// View Model to handle displaying the <see cref="Scene"/>
    /// </summary>
    public class SceneViewModel : BaseViewModel
    {
        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public SceneViewModel()
        {
            // Initialize the Scene
            Scene = new Scene();

            // Construct the graphics overlay collection
            _graphicsOverlays = new GraphicsOverlayCollection();

            // Construct the route endpoints graphics overlay
            _routeEndpointGraphics = new GraphicsOverlay();
            _routeEndpointGraphics.SceneProperties.SurfacePlacement = SurfacePlacement.Absolute;

            // Add the relevant route graphics to the collection
            _graphicsOverlays.Add(_routeEndpointGraphics);

            // Load required data needed
            LoadScene();

            PropertyChanged += RoomSelectedEventHandler;
        }

        /// <summary>
        /// Event handler to compute the route if it is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomSelectedEventHandler(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(_routeEndpointGraphics))
                return;

            // Only try to find the route when we have the rooms selected and the graphics are displayed
            if (!SelectedStartRoom.IsNullOrEmpty() && !SelectedDestinationRoom.IsNullOrEmpty() && _routeEndpointGraphics.Graphics.Count() >= 2)
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await FindRoute());
        }

        private async Task FindRoute()
        {
            if (_routing)
                return;
            try
            {
                _routing = true;

                // Extract the stops from the Scene View Model
                List<Stop> stops = GetStops();

                // If there aren't enough stops, don't try to route
                if (stops.Count < 2)
                    return;

                // Clear the existing graphics
                if (GraphicsOverlays.Contains(_routeGraphics))
                    GraphicsOverlays.Remove(_routeGraphics);

                // Computer the route
                _routeGraphics = await RouteSolver.Solve(Scene.TransportationNetworks?.FirstOrDefault(), stops, TravelMode, RouteRestriction);
                GraphicsOverlays.Add(_routeGraphics);

                // Set the envelope to the extent of the route
                Envelope = GeometryHelpers.ConstructBoundingEnvelope(_routeGraphics.Graphics.Select(g => g.Geometry), scaleFactor: 2.0);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Could not solve route");
            }
            finally
            {
                _routing = false;
            }
        }

        internal void ResetCamera()
        {
            Camera = defaultCamera;
            OnPropertyChanged(nameof(Camera));
        }

        internal async Task ZoomToRoute()
        {
            if (_routeGraphics != null)
            {
                Envelope = GeometryHelpers.ConstructBoundingEnvelope(_routeGraphics.Graphics.Select(graphic => graphic.Geometry));
            }
            else
            {
                if (!SelectedStartRoom.IsNullOrEmpty() && !SelectedDestinationRoom.IsNullOrEmpty())
                    Envelope = GeometryHelpers.ConstructBoundingEnvelope(_routeEndpointGraphics.Graphics.Select(graphic => graphic.Geometry));
                else
                    await SetViewpointOnBuildingSelected();
            }
        }

        #endregion

        #region Private Properties

        #region Private Routing Properties

        /// <summary>
        /// Private variable for the restrictions placed on the route, default to none
        /// </summary>
        private RouteRestrictions _routeRestriction { get; set; } = RouteRestrictions.None;

        /// <summary>
        /// Private property for the route travel mode, default to fastest
        /// </summary>
        private TravelModes _travelMode { get; set; } = TravelModes.Fastest;

        /// <summary>
        /// Graphics overlay for the endpoints of the route
        /// </summary>
        private GraphicsOverlay _routeEndpointGraphics { get; set; }

        /// <summary>
        /// Graphics overlay for the route itself
        /// </summary>
        private GraphicsOverlay _routeGraphics { get; set; }

        /// <summary>
        /// Flag for if the routing task is already taking place
        /// </summary>
        private bool _routing = false;

        #endregion

        #region Miscellaneous

        /// <summary>
        /// Private variable which will be set when an operation is being perform as to not do things multiple times
        /// </summary>
        private bool _busy { get; set; } = false;

        /// <summary>
        /// Private scene variable
        /// </summary>
        private Scene _scene { get; set; }

        /// <summary>
        /// Private variable to show no features in a layer
        /// </summary>
        private static string _showNoFeaturesExpression = "OBJECTID IS NULL";

        #endregion

        #region Private SceneView Properties

        /// <summary>
        /// Private camera variable
        /// </summary>
        private Camera _camera { get; set; } = defaultCamera;

        /// <summary>
        /// The envelope that contains the start and destination buildings
        /// </summary>
        private Envelope _envelope { get; set; }

        /// <summary>
        /// The straight line path between the start and destination buildings
        /// </summary>
        private LineSegment _lineSegment { get; set; }

        /// <summary>
        /// Any graphics to be displayed on the scene
        /// </summary>
        private GraphicsOverlayCollection _graphicsOverlays { get; set; }

        #endregion

        #region Private Feature Layer Properties

        /// <summary>
        /// The Building Outlines feature layer with a solid fill
        /// </summary>
        private static FeatureLayer _buildingsSolidLayer { get; set; } = new FeatureLayer();

        /// <summary>
        /// The building outlines feature layer with no fill
        /// </summary>
        private static FeatureLayer _buildingsWireframeLayer { get; set; } = new FeatureLayer();

        /// <summary>
        /// The rooms feature layer
        /// </summary>
        private static FeatureLayer _roomsLayer { get; set; } = new FeatureLayer();

        /// <summary>
        /// The endpoints feature layer
        /// </summary>
        private static FeatureLayer _endpointsLayer { get; set; }

        #endregion

        #region Private Building Properies

        private SortedDictionary<string, string> _startBuildingOptions { get; set; }
        private SortedDictionary<string, string> _destinationBuildingOptions { get; set; }
        private string _selectedStartBuildingKey { get; set; }
        private string _selectedDestinationBuildingKey { get; set; }

        #endregion

        #region Private Room Properies

        private Dictionary<string, string> _startBuildingRoomOptions { get; set; }
        private Dictionary<string, string> _destinationBuildingRoomOptions { get; set; }
        private string _selectedStartRoomKey { get; set; }
        private string _selectedDestinationRoomKey { get; set; }

        #endregion

        #region Private Building Floor Properies

        private SortedDictionary<int, string> _startBuildingFloorOptions { get; set; }
        private SortedDictionary<int, string> _destinationBuildingFloorOptions { get; set; }
        private int _selectedStartFloorKey { get; set; } = -1;
        private int _selectedDestinationFloorKey { get; set; } = -1;

        #endregion

        #endregion

        #region Public Properties

        #region Misc

        /// <summary>
        /// Default camera position for when the application first loads
        /// </summary>
        public readonly static Camera defaultCamera = new Camera(new MapPoint(-112.8523, 49.6758, 1222.2906), 274.3300, 70.0749, 0);

        /// <summary>
        /// Public getter for the busy flag
        /// </summary>
        public bool Busy
        {
            get { return _busy; }
            private set { _busy = value; }
        }

        /// <summary>
        /// Public getter/setter for the scene
        /// </summary>
        public Scene Scene
        {
            get { return _scene; }
            set { _scene = value; }
        }

        /// <summary>
        /// Public getter for the buildings layer
        /// </summary>
        public FeatureLayer BuildingsSolidLayer => _buildingsSolidLayer;

        /// <summary>
        /// Public getter for the rooms layer
        /// </summary>
        public FeatureLayer RoomsLayer => _roomsLayer;

        #endregion

        #region Public Routing Properties 
        
        /// <summary>
        /// Public getter for the stops of the route
        /// </summary>
        /// <returns></returns>
        public List<Stop> GetStops() => _routeEndpointGraphics.Graphics.Select(graphic => new Stop((MapPoint)graphic.Geometry)).ToList();

        /// <summary>
        /// Public getter/setter for the route restrictions. Setting property will re-calculate the route
        /// </summary>
        public RouteRestrictions RouteRestriction
        {
            get { return _routeRestriction; }
            set 
            { 
                _routeRestriction = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await FindRoute());
            }
        }

        /// <summary>
        /// Public getter/setter for the route travel mode. Setting property will re-calculate the route
        /// </summary>
        public TravelModes TravelMode
        {
            get { return _travelMode; }
            set 
            { 
                _travelMode = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await FindRoute());
            }
        }

        #endregion

        #region Public SceneView Properties

        /// <summary>
        /// Public getter and private setter for the camera property, which the SceneView is bound too
        /// </summary>
        public Camera Camera
        {
            get { return _camera; }
            private set { _camera = value; }
        }

        /// <summary>
        /// Public getter/setter for the envelope that contains the start and destination buildings
        /// </summary>
        public Envelope Envelope
        {
            get { return _envelope; }
            set { _envelope = value; }
        }

        /// <summary>
        /// Public getter/setter for the line segment connecting the start and destination points
        /// </summary>
        public LineSegment LineSegment
        {
            get { return _lineSegment; }
            set { _lineSegment = value; }
        }

        /// <summary>
        /// Public getter/setter for any graphics to be displayed on the map
        /// </summary>
        public GraphicsOverlayCollection GraphicsOverlays
        {
            get { return _graphicsOverlays; }
            set { _graphicsOverlays = value; }
        }

        #endregion

        #region Public Building Selection Properties

        public SortedDictionary<string, string> StartBuildingOptions
        {
            get { return _startBuildingOptions; }
            set
            {
                _startBuildingOptions = value;

            }
        }
        public SortedDictionary<string, string> DestinationBuildingOptions
        {
            get { return _destinationBuildingOptions; }
            set
            {
                _destinationBuildingOptions = value;

            }
        }
        public string SelectedStartBuilding
        {
            get { return _selectedStartBuildingKey; }
            set
            {
                _selectedStartBuildingKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await SetViewpointOnBuildingSelected());

            }
        }
        public string SelectedDestinationBuilding
        {
            get { return _selectedDestinationBuildingKey; }
            set
            {
                _selectedDestinationBuildingKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await SetViewpointOnBuildingSelected());
            }
        }

        #endregion

        #region Public Room Selection Properties

        public Dictionary<string, string> StartBuildingRoomOptions
        {
            get { return _startBuildingRoomOptions; }
            set
            {
                _startBuildingRoomOptions = value;
            }
        }
        public Dictionary<string, string> DestinationBuildingRoomOptions
        {
            get { return _destinationBuildingRoomOptions; }
            set
            {
                _destinationBuildingRoomOptions = value;
            }
        }
        public string SelectedStartRoom
        {
            get { return _selectedStartRoomKey; }
            set
            {
                _selectedStartRoomKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await SetViewpointOnRoomSelected(_selectedStartRoomKey, BuildingType.Start));
            }
        }
        public string SelectedDestinationRoom
        {
            get { return _selectedDestinationRoomKey; }
            set
            {
                _selectedDestinationRoomKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(async () => await SetViewpointOnRoomSelected(_selectedDestinationRoomKey, BuildingType.Destination));
            }
        }

        #endregion

        #region Public Floor Selection Properties

        public SortedDictionary<int, string> StartBuildingFloorOptions
        {
            get { return _startBuildingFloorOptions; }
            set
            {
                _startBuildingFloorOptions = value;
            }
        }
        public SortedDictionary<int, string> DestinationBuildingFloorOptions
        {
            get { return _destinationBuildingFloorOptions; }
            set
            {
                _destinationBuildingFloorOptions = value;
            }
        }
        public int SelectedStartBuildingFloor
        {
            get { return _selectedStartFloorKey; }
            set
            {
                _selectedStartFloorKey = value;
                if (_selectedStartBuildingKey == _selectedDestinationBuildingKey)
                    _selectedDestinationFloorKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(() => UpdateRoomsDefinitionQuery());
            }
        }
        public int SelectedDestinationBuildingFloor
        {
            get { return _selectedDestinationFloorKey; }
            set
            {
                _selectedDestinationFloorKey = value;
                if (_selectedStartBuildingKey == _selectedDestinationBuildingKey)
                    _selectedStartFloorKey = value;
                Dispatcher.CurrentDispatcher.InvokeAsync(() => UpdateRoomsDefinitionQuery());
            }
        }

        #endregion

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets the definition queries of the solid and wire buildings according to the 
        /// selected start/destination buildings
        /// </summary>
        private void UpdateBuildingsDefinitionQueries()
        {
            // Escape single quotes
            var selectedStartBuilding = SelectedStartBuilding.EscapeSingleQuotes();
            var selectedDestinationBuilding = SelectedDestinationBuilding.EscapeSingleQuotes();

            // Create list of selected buildings
            List<string> selectedBuilings = new List<string>();
            if (!selectedStartBuilding.IsNullOrEmpty())
                selectedBuilings.Add(selectedStartBuilding.WrapInSingleQuotes());
            if (!selectedDestinationBuilding.IsNullOrEmpty())
                selectedBuilings.Add(selectedDestinationBuilding.WrapInSingleQuotes());

            // Set the solid fill buildings definition query to exclude selected buildings
            var excludedBuildings = selectedBuilings.Count() == 0
                ? string.Empty
                : string.Format("BNAME NOT IN ({0})", string.Join(",", selectedBuilings));

            _buildingsSolidLayer.DefinitionExpression = excludedBuildings;

            // Set the wire frame buildings definitiion query to include selected buildings
            var includedBuildings = selectedBuilings.Count() == 0
                ? string.Empty
                : string.Format("BNAME IN ({0})", string.Join(",", selectedBuilings));

            if (!includedBuildings.IsNullOrEmpty())
            {
                _buildingsWireframeLayer.DefinitionExpression = includedBuildings;
                _buildingsWireframeLayer.IsVisible = true;
            }
            else
                _buildingsWireframeLayer.IsVisible = false;

            UpdateRoomsDefinitionQuery();
        }

        /// <summary>
        /// Sets the definition query for the rooms layer based on the combinations of 
        /// selected start/destination building, and the floors selected for each building as well
        /// </summary>
        private void UpdateRoomsDefinitionQuery()
        {
            // Escape single quotes
            var selectedStartBuilding = SelectedStartBuilding.EscapeSingleQuotes();
            var selectedDestinationBuilding = SelectedDestinationBuilding.EscapeSingleQuotes();

            // Set the building name and relevant floors for the starting building
            string startFloorQuery = "";
            if (!selectedStartBuilding.IsNullOrEmpty())
            {
                startFloorQuery = "(BNAME = " + selectedStartBuilding.WrapInSingleQuotes();
                if (SelectedStartBuildingFloor != -1)
                    startFloorQuery += " AND FLOOR = " + SelectedStartBuildingFloor;
                startFloorQuery += ")";
            }

            // Set the building name and relevant floors for the destination building
            // Only if they are different buildings
            string destinationFloorQuery = "";
            if (!selectedDestinationBuilding.IsNullOrEmpty() && selectedDestinationBuilding != selectedStartBuilding)
            {
                destinationFloorQuery = "(BNAME = " + selectedDestinationBuilding.WrapInSingleQuotes();
                if (SelectedDestinationBuildingFloor != -1)
                    destinationFloorQuery += " AND FLOOR = " + SelectedDestinationBuildingFloor;
                destinationFloorQuery += ")";
            }

            // Join the two expressions together 
            // If both expressions have a value, join them with an OR, else just show the relevant expression
            var floorQuery = string.Format("{0} {1} {2}",
                startFloorQuery,
                (!startFloorQuery.IsNullOrEmpty() && !destinationFloorQuery.IsNullOrEmpty()) ? "OR" : "",
                destinationFloorQuery);

            _roomsLayer.DefinitionExpression = string.IsNullOrWhiteSpace(floorQuery) ? _showNoFeaturesExpression : floorQuery;
        }

        /// <summary>
        /// Clears the current settings and returns to original viewpoint
        /// </summary>
        internal void Clear()
        {
            // Set the selected options to the default value
            SelectedStartBuilding = string.Empty;
            SelectedDestinationBuilding = string.Empty;
            SelectedStartBuildingFloor = -1;
            SelectedDestinationBuildingFloor = -1;
            SelectedStartRoom = string.Empty;
            SelectedDestinationRoom = string.Empty;

            // Clear the floor options and selected options
            StartBuildingFloorOptions?.Clear();
            DestinationBuildingFloorOptions?.Clear();

            // Clear the room options
            StartBuildingRoomOptions?.Clear();
            DestinationBuildingRoomOptions?.Clear();

            // Clear the graphics
            GraphicsOverlays.ToList().ForEach(graphicsOverlay => graphicsOverlay.Graphics?.Clear());
        }

        #region Feature Layer Methods

        /// <summary>
        /// Event listener for when the Building Outlines are done loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async Task LoadBuildingOptions()
        {
            // Set the building options 
            var queryParams = new QueryParameters();
            var buildingOptions = await _buildingsSolidLayer.FeatureTable.QueryFeaturesAsync(queryParams);

            SortedDictionary<string, string> buildings = buildingOptions
                .ToDictionary(building => building.Attributes["BNAME"].ToString(), building => building.Attributes["BNAME"].ToString())
                .ToSortedDictionary();

            StartBuildingOptions = buildings;
            DestinationBuildingOptions = buildings;
        }

        /// <summary>
        /// Loads the required data for the scene, adds the layers, and sets the initial viewport
        /// </summary>
        private async void LoadScene()
        {
            // Get the scene from the mobile scene package
            Scene = await DataManager.LoadScene();
            //Scene.Basemap = null;

            // Load in the buildings layers
            _buildingsSolidLayer = await DataManager.LoadSolidBuildings(Scene);
            _buildingsWireframeLayer = await DataManager.LoadWireframeBuildings(Scene);

            // Populate the drop down options
            await LoadBuildingOptions();

            // Load in the rooms and endpoints layer
            _roomsLayer = await  DataManager.LoadRooms(Scene);
            _endpointsLayer = await DataManager.LoadEndpoints();
        }

        #endregion

        /// <summary>
        /// Query the room layer
        /// </summary>
        /// <param name="roomGUID">The GUID of the room</param>
        /// <returns>List of rooms that have that GUID</returns>
        private static async Task<IEnumerable<Feature>> QueryRooms(string roomGUID)
        {
            // Create the query parameters for the room layer
            var query = new QueryParameters()
            {
                WhereClause = "GUID = '" + roomGUID + "'"
            };

            // Get the features
            var roomQuery = await _roomsLayer.FeatureTable.QueryFeaturesAsync(query);

            return roomQuery;
        }

        /// <summary>
        /// Set the appropriate viewpoint for the selected room and adds the room to the <see cref="_roomsLayer"/> selection
        /// </summary>
        /// <param name="roomGUID"></param>
        /// <returns></returns>
        private async Task SetViewpointOnRoomSelected(string roomGUID, BuildingType buildingType)
        {
            if (roomGUID.IsNullOrEmpty())
                return;

            // Get the appropriate room
            var roomFeatures = await QueryRooms(roomGUID);

            // If the room could not be found, show a message
            if (roomFeatures.Count() < 1)
            {
                MessageBox.Show(string.Format("Could not find room with ID {0}", roomGUID), "Something went wrong");
                return;
            }

            // Set the marker for the room
            await SetRouteGraphics(roomGUID, buildingType);
        }

        /// <summary>
        /// Set the graphic for the room
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        private async Task SetRouteGraphics(string roomGUID, BuildingType buildingType)
        {
            // create the query for the room
            var endpointQuery = new QueryParameters()
            {
                WhereClause = "ROOMGUID = '" + roomGUID + "'"
            };

            // Query the endpoints
            var endpointResult = await _endpointsLayer.FeatureTable.QueryFeaturesAsync(endpointQuery);

            // Get the first result
            var endpoint = endpointResult.FirstOrDefault();

            // Return if no endpoint found
            if (endpoint == default(Feature))
                return;

            // Get the location of the endpoint
            var roomLocation = endpoint.Geometry as MapPoint;

            // Set the color to red if it is the start, or green if it is the destination
            var markerColor = buildingType == BuildingType.Start ? Color.Red : Color.Green;

            // Create marker
            SimpleMarkerSceneSymbol marker = new SimpleMarkerSceneSymbol(SimpleMarkerSceneSymbolStyle.Diamond, markerColor, 2, 1, 1, SceneSymbolAnchorPosition.Bottom);

            // Find any existing symbols with the same color and remove them
            var existingGraphic = _routeEndpointGraphics.Graphics.FirstOrDefault(graphic => ((SimpleMarkerSceneSymbol)graphic.Symbol).Color.Equals(marker.Color));
            if (existingGraphic != default(Graphic))
                _routeEndpointGraphics.Graphics.Remove(existingGraphic);

            // Add the new marker and trigger the graphics overlay to update
            _routeEndpointGraphics.Graphics.Add(new Graphic(roomLocation, marker));
            OnPropertyChanged(nameof(_routeEndpointGraphics));
        }

        /// <summary>
        /// Return a query of the buildings layer based on the building name
        /// </summary>
        /// <param name="buildingName">Name of the building</param>
        /// <returns></returns>
        private async Task<List<Feature>> QueryBuildingOutlines(string buildingName)
        {
            // Set the query parameter to select the building with the same name
            var query = QueryHelpers.BuildBuildingNameQueryParameters(buildingName);

            // Query the buildings for the matching name
            var queryResult = await _buildingsSolidLayer.FeatureTable.QueryFeaturesAsync(query);

            return queryResult.ToList();
        }

        /// <summary>
        /// Queries the buildings layer for the start and destination building,
        /// gets the appropriate envelope for the selected buildings and sets it,
        /// and triggers the updating of the appropriate floor and room data
        /// </summary>
        private async Task SetViewpointOnBuildingSelected()
        {
            // Initialize envelope features
            IEnumerable<Feature> features = new List<Feature>();

            // Get starting features
            IEnumerable<Feature> startingFeatures = await QueryBuildingOutlines(SelectedStartBuilding.EscapeSingleQuotes());

            // Get destination features
            IEnumerable<Feature> destinationFeatures = await QueryBuildingOutlines(SelectedDestinationBuilding.EscapeSingleQuotes());

            // Add starting and destination features to list
            features = features.Concat(startingFeatures);
            features = features.Concat(destinationFeatures);

            // Update the definition queries to show the appropriate features
            UpdateBuildingsDefinitionQueries();

            // If there are features, construct the envelope that ensloses them and select them
            if (features.Count() > 0)
            {
                // Calculate envelope of features
                Envelope = await GeometryHelpers.ConstructBoundingEnvelope(features, computeZ: true, baseSurface: Scene.BaseSurface);

                // Add the features to the wireframes selection as well
                //_buildingsWireframeLayer.ClearSelection();
                //_buildingsWireframeLayer.SelectFeatures(features);
            }

            // Set the list options for the building rooms and floor
            if (startingFeatures.Count() > 0)
            {
                await SetBuildingFloorOptions(startingFeatures, BuildingType.Start);
                await SetRoomOptions(BuildingType.Start);
            }
            if (destinationFeatures.Count() > 0)
            {
                await SetBuildingFloorOptions(destinationFeatures, BuildingType.Destination);
                await SetRoomOptions(BuildingType.Destination);
            }
        }

        /// <summary>
        /// Sets the values that can be used in the <see cref="SelectedStartBuildingFloor"/> or <see cref="SelectedDestinationBuildingFloor"/>
        /// </summary>
        /// <param name="startingFeatures">List of building features to build the list off of</param>
        /// <param name="buildingType">The building type, which determines which list to populate</param>
        /// <returns></returns>
        private async Task SetBuildingFloorOptions(IEnumerable<Feature> buildingFeatures, BuildingType buildingType)
        {
            // Get the selected building feature
            var selectedBuilding = buildingFeatures.First();

            // Get the name of the selected building
            var buildingName = selectedBuilding.Attributes["BNAME"].ToString();

            // Get the query for the building name
            var query = QueryHelpers.BuildBuildingNameQueryParameters(buildingName.EscapeSingleQuotes());

            // Get the rooms for the selected building
            var roomsForBuilding = await _roomsLayer.FeatureTable.QueryFeaturesAsync(query);

            // Create a list of the distinct  floors based on the rooms features
            // and convert it to a dictionary
            SortedDictionary<int, string> floors = roomsForBuilding
                .Select(room => room.Attributes["FLOOR"])
                .Distinct()
                .ToDictionary(floor => Convert.ToInt32(floor), floor => floor.ToString())
                .ToSortedDictionary();

            // If dictionary only contains one item, change value to "All"
            // Else, just add the "All" value to the dictionary
            if (floors.Count() == 1)
                floors = new SortedDictionary<int, string> { { -1, "All" } };
            else
                floors.Add(-1, "All");

            // Set the appropriate options and reset the selected floor
            if (buildingType == BuildingType.Start)
            {
                SelectedStartBuildingFloor = -1;
                StartBuildingFloorOptions = floors;
            }
            else
            {
                SelectedDestinationBuildingFloor = -1;
                DestinationBuildingFloorOptions = floors;
            }
        }

        /// <summary>
        /// Set the possible room options from the rooms layer for the appropriate building
        /// </summary>
        /// <param name="buildingType">Either the start or destination building</param>
        /// <returns></returns>
        private async Task SetRoomOptions(BuildingType buildingType)
        {
            // Build the query
            var query = QueryHelpers.BuildBuildingNameQueryParameters(buildingType == BuildingType.Start
                ? SelectedStartBuilding.EscapeSingleQuotes()
                : SelectedDestinationBuilding.EscapeSingleQuotes()
            );

            try
            {
                // Whenever the room options are re-built, we must have switched buildings
                // thus, we remove any route graphics
                if (_routeGraphics != null && GraphicsOverlays.Contains(_routeGraphics))
                    GraphicsOverlays.Remove(_routeGraphics);

                // Query the endpoints layer
                var endpointResults = await _endpointsLayer.FeatureTable.QueryFeaturesAsync(query);

                // Filter out the endpoints with no Room GUID
                var filteredEnpoints = endpointResults.Where(room => !room.Attributes["ROOMGUID"].ToString().IsNullOrEmpty())
                        .Distinct(new FeatureCompararer("ROOMGUID"))
                        .OrderByDescending(f => f.Attributes["ISDEFAULT"])
                        .ThenBy(f => f.Attributes["ROOMNUM"])
                        .ThenBy(f => f.Attributes["FID"]);

                // Get the default room for the building
                var defaultRoom = endpointResults
                    .FirstOrDefault(room => Convert.ToInt32(room.Attributes["ISDEFAULT"]) != 0)
                    ?.GetAttributeValue("ROOMGUID")
                    .ToString() ?? string.Empty;

                // Create a dictionary of the filtered endpoints
                var rooms = filteredEnpoints.ToDictionary(room => room.Attributes["ROOMGUID"].ToString(), room =>
                    {
                        var attr = room.Attributes;
                        var roomString = "";

                        // If there is a room alias
                        if (!attr["ROOMALIAS"].ToString().IsNullOrEmpty())
                        {
                            // Set the string to the alias
                            roomString = attr["ROOMALIAS"].ToString();

                            // If there is also a room number, append the room number in paratheses
                            if (!attr["ROOMNUM"].ToString().IsNullOrEmpty())
                                roomString += string.Format(" ({0})", attr["ROOMNUM"].ToString());
                        }
                        // If no alias, but a room number, display the room number
                        else if (!attr["ROOMNUM"].ToString().IsNullOrEmpty())
                            roomString = attr["ROOMNUM"].ToString();
                        // If no room number, just show the FID
                        else
                            roomString = string.Format("Room {0}", attr["FID"].ToString());

                        return roomString;
                    });

                // Set the room options property
                if (buildingType == BuildingType.Start)
                {
                    if (_selectedStartRoomKey.IsNullOrEmpty())
                    {
                        _selectedStartRoomKey = defaultRoom;
                        await SetRouteGraphics(defaultRoom, BuildingType.Start);
                    }
                    StartBuildingRoomOptions = rooms;
                }
                else
                {
                    DestinationBuildingRoomOptions = rooms;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Couldn't create list of rooms");
            }
        }

        #endregion
    }


    /// <summary>
    /// Class used to compare <see cref="Feature"/>
    /// </summary>
    public class FeatureCompararer : IEqualityComparer<Feature>
    {
        private string _fieldName;

        public FeatureCompararer(string fieldName)
        {
            _fieldName = fieldName;
        }

        public bool Equals(Feature f1, Feature f2)
        {
            return f1.Attributes[_fieldName].ToString() == f1.Attributes[_fieldName].ToString();
        }

        public int GetHashCode(Feature obj)
        {
            var hashCode = obj.Attributes[_fieldName];
            return hashCode.GetHashCode();
        }
    }
}