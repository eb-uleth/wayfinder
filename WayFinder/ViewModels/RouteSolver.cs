﻿using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks.NetworkAnalysis;
using Esri.ArcGISRuntime.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WayFinder
{
    /// <summary>
    /// Handles the task of routing between stops
    /// </summary>
    public static class RouteSolver
    {
        /// <summary>
        /// Finds a route and computes a graphics overlay for the the line
        /// </summary>
        /// <param name="networkDataset">Network dataset used to find the route</param>
        /// <param name="stops">The end points of the route</param>
        /// <returns></returns>
        internal static async Task<GraphicsOverlay> Solve(TransportationNetworkDataset networkDataset, List<Stop> stops, TravelModes travelMode, RouteRestrictions routeRestriction)
        {
            // Create the route task
            RouteTask _routeTask = await RouteTask.CreateAsync(networkDataset);

            // Get the default route parameters for the route task
            RouteParameters routeParams = await _routeTask.CreateDefaultParametersAsync();
            routeParams.ReturnStops = true;

            // Get any appropriate restrictions
            if (routeRestriction != RouteRestrictions.None)
            {
                // Set the value to query the paths layer against
                string restrictionPathType = routeRestriction == RouteRestrictions.AvoidElevators ? "Elevator" : "Stairs";

                // Get the appropriate polyline features from the Path layer
                GeodatabaseFeatureTable paths = networkDataset.Geodatabase.GeodatabaseFeatureTables.FirstOrDefault(featureTable => featureTable.DisplayName == "Paths");

                // If we can find the paths layer
                if (paths != null)
                {
                    // Build a new query
                    var query = new QueryParameters()
                    {
                        WhereClause = "PATHTYPE = '" + restrictionPathType + "'"
                    };

                    // Query the table
                    var barrierResult = await paths.QueryFeaturesAsync(query);

                    // Create barriers from the result
                    List<PolylineBarrier> barriers = barrierResult.Select(feature => new PolylineBarrier((Polyline)feature.Geometry)).ToList();

                    // Add the barriers to the route
                    routeParams.SetPolylineBarriers(barriers);
                }
            }

            // Apply the travel mode
            Esri.ArcGISRuntime.Tasks.NetworkAnalysis.TravelMode mode =  _routeTask.RouteTaskInfo.TravelModes.FirstOrDefault(_mode =>
            {
                if (Enum.TryParse(_mode.Name, out TravelModes tMode))
                {
                    return tMode == travelMode;
                }
                return false;
            }); 

            if (mode != null)
                routeParams.TravelMode = mode;

            // Set the route stops
            routeParams.SetStops(stops);

            // Solve the route
            RouteResult result = await _routeTask.SolveRouteAsync(routeParams);

            // Get the first route result
            Route route = result.Routes.First();

            // Get the route geometry
            Polyline routePolyline = route.RouteGeometry;

            // Create the output graphics overlay
            GraphicsOverlay routeGraphics = new GraphicsOverlay();

            // Set the surface properties to show the route above the ground
            routeGraphics.SceneProperties.SurfacePlacement = SurfacePlacement.Absolute;

            SolidStrokeSymbolLayer lineSymbol = new SolidStrokeSymbolLayer(1, Color.FromArgb(255, 0, 255, 0), new List<GeometricEffect>(), StrokeSymbolLayerLineStyle3D.Tube);

            List<SolidStrokeSymbolLayer> symbolLayers = new List<SolidStrokeSymbolLayer>() { lineSymbol };

            MultilayerPolylineSymbol multilayerPolylineSymbol = new MultilayerPolylineSymbol(symbolLayers);

            Graphic routeGraphic = new Graphic(routePolyline, multilayerPolylineSymbol)
            {
                ZIndex = 1
            };

            routeGraphics.Graphics.Add(routeGraphic);

            return routeGraphics;
        }

    }
}
