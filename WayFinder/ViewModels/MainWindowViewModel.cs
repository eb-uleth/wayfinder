﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WayFinder
{
    /// <summary>
    /// View model for the main window
    /// </summary>
    public class MainWindowViewModel : BaseViewModel
    {

        #region Private Properties

        #endregion

        #region Public Properties

        public SceneViewModel SceneViewModel { get; set; }

        /// <summary>
        /// Flag to determine if a loading operation is being performed
        /// </summary>
        public bool IsLoading { get; protected set; } = true;

        #endregion

        #region Commands

        /// <summary>
        /// Command to clear the selected buildings
        /// </summary>
        public ICommand ClearCommand { get; set; }

        /// <summary>
        /// Command to zoom to the approprate route features
        /// </summary>
        public ICommand ZoomToRouteCommand { get; set; }

        /// <summary>
        /// Command to compute and find the route
        /// </summary>
        public ICommand HomeCommand { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindowViewModel()
        {
            // Construct the SceneViewModel
            SceneViewModel = new SceneViewModel();

            // Set up commands
            ClearCommand = new RelayCommand(Clear);
            ZoomToRouteCommand = new RelayCommand(async () => await PerformZoom());
            HomeCommand = new RelayCommand(GoToHome);

            SceneViewModel.Scene.Loaded += Scene_Loaded;
        }

        private void Scene_Loaded(object sender, EventArgs e)
        {
            IsLoading = false;
        }

        #endregion

        #region Private Members

        /// <summary>
        /// Method to perform requisite logic to clear the window
        /// </summary>
        private void Clear()
        {
            // Call the clear method on the SceneViewModel
            SceneViewModel.Clear();
        }

        /// <summary>
        /// Calls methods on the <see cref="SceneViewModel"/> to zoom to appropriate features
        /// </summary>
        private async Task PerformZoom()
        {
            await SceneViewModel.ZoomToRoute();
        }

        /// <summary>
        /// Calls the appropriate methods on the <see cref="SceneViewModel"/> to compute the route
        /// </summary>
        /// <returns></returns>
        private void GoToHome()
        {
            SceneViewModel.ResetCamera();
        }

        #endregion

    }
}
