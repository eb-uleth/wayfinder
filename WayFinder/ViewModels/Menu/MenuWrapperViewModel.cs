﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace WayFinder
{
    /// <summary>
    /// Wrapper around a menu 
    /// </summary>
    public class MenuWrapperViewModel : BaseViewModel
    {
        #region Public Properties

        /// <summary>
        /// The menu content
        /// </summary>
        public BaseViewModel Content { get; set; }

        /// <summary>
        /// Flag to determine if the menu is collapsed
        /// </summary>
        public bool IsCollapsed { get; private set; } = false;

        /// <summary>
        /// The size of the tab to collapse the menu
        /// </summary>
        public int CollapsedTabSize { get; } = 50;

        #endregion

        #region Commands

        /// <summary>
        /// Command to collapse the menu wrapper
        /// </summary>
        public ICommand CollapseCommand { get; set; }

        #endregion

        #region Constructor

        public MenuWrapperViewModel()
        {
            CollapseCommand = new RelayCommand(Collapse);
        }

        #endregion

        #region Private Commands

        /// <summary>
        /// Internal command to collapse the menu
        /// </summary>
        /// <returns></returns>
        private void Collapse()
        {
            IsCollapsed ^= true;
        }

        #endregion
    }
}
