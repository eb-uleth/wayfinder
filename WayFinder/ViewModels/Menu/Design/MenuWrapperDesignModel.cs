﻿using System.Collections.Generic;

namespace WayFinder
{
    /// <summary>
    /// Design-time data for a <see cref="MenuWrapperViewModel"/>
    /// </summary>
    public class MenuWrapperDesignModel : MenuWrapperViewModel
    {
        #region Singleton

        /// <summary>
        /// A static instance of the type
        /// </summary>
        public static MenuWrapperDesignModel Instance => new MenuWrapperDesignModel();

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MenuWrapperDesignModel()
        {
        }

        #endregion
    }
}
