﻿using System.Collections.Generic;

namespace WayFinder
{
    /// <summary>
    /// Collection of language extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Converts a <see cref="Dictionary{TKey, TValue}"/> to a <see cref="SortedDictionary{TKey, TValue}"/>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="existing"></param>
        /// <returns></returns>
        public static SortedDictionary<TKey, TValue> ToSortedDictionary<TKey, TValue>(this Dictionary<TKey, TValue> existing)
        {
            return new SortedDictionary<TKey, TValue>(existing);
        }

        /// <summary>
        /// Return the input string wrapped in single quotes
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string WrapInSingleQuotes(this string inputString)
        {
            if (inputString == null)
                return inputString;
            return "'" + inputString + "'";
        }

        /// <summary>
        /// Replaces any single quotes (') with the escaped version ('')
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string EscapeSingleQuotes(this string inputString)
        {
            if (inputString == null)
                return inputString;

            return inputString.Replace("'", "''");
        }

        public static bool IsNullOrEmpty(this string inputString)
        {
            return string.IsNullOrEmpty(inputString);
        }
    }
}
