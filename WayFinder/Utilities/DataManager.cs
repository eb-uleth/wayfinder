﻿using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Portal;
using Esri.ArcGISRuntime.Symbology;
using System;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WayFinder
{
    /// <summary>
    /// Collection of methods to load the requisite data from the resources directory
    /// </summary>
    public static class DataManager
    {
        /// <summary>
        /// Returns the path to the data directory for the application
        /// </summary>
        /// <returns></returns>
        public static string GetDataDirectory()
        {
            // Get the current working directory 
            string appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

            // Get the current project directory
            string dataDirectory = Path.Combine(appDataFolder, "WayfinderData");

            // Create the directory if it doesn't exist
            if (!Directory.Exists(dataDirectory))
                Directory.CreateDirectory(dataDirectory);

            return dataDirectory;
        }

        /// <summary>
        /// Downloads the portal item and puts its contents in a directory in the data directory for the application
        /// </summary>
        /// <param name="portalItemId"></param>
        /// <returns></returns>
        public static async Task DownloadPortalItem(string portalItemId)
        {
            ArcGISPortal portal = await ArcGISPortal.CreateAsync();

            PortalItem item = await PortalItem.CreateAsync(portal, portalItemId);

            Task<Stream> downloadTask = item.GetDataAsync();

            string outputDirectory = Path.Combine(GetDataDirectory(), item.ItemId);

            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            string outputFile = Path.Combine(outputDirectory, item.Name);

            if (!File.Exists(outputFile))
            {
                using (var s = await downloadTask.ConfigureAwait(false))
                {
                    using (var f = File.Create(outputFile))
                    {
                        await s.CopyToAsync(f).ConfigureAwait(false);
                    }
                }
            }

            // Unzip the file if it is a zip archive.
            if (outputFile.EndsWith(".zip"))
            {
                await UnpackData(outputFile, outputDirectory);
            }
        }

        /// <summary>
        /// Unzips the file at path defined by <paramref name="zipFile"/>
        ///  into <paramref name="folder"/>.
        /// </summary>
        /// <param name="zipFile">Path to the zip archive to extract.</param>
        private static Task UnpackData(string zipFile, string folder)
        {
            return Task.Run(() =>
            {
                using (var archive = ZipFile.OpenRead(zipFile))
                {
                    foreach (var entry in archive.Entries.Where(m => !string.IsNullOrWhiteSpace(m.Name)))
                    {
                        string path = Path.Combine(folder, entry.FullName);
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                        entry.ExtractToFile(path, true);
                    }
                }
            });
        }

        public static async Task<string> GetData(string itemId, params string[] pathParts)
        {
            string path = Path.Combine(GetDataDirectory(), itemId, Path.Combine(pathParts));

            if (!File.Exists(path))
            {
                await DownloadPortalItem(itemId);
            }

            return path;
        }

        /// <summary>
        /// Loads the scene from the mobile scene package shipped with the project
        /// </summary>
        /// <returns></returns>
        public static async Task<Scene> LoadScene()
        {
            // Create new scene
            Scene scene = new Scene();

            try
            {
                // Get the path to the mobile scene package
                var mobileScenePackagePath = await GetData("1d2c1314daab4ad5803d120f6daa312d", "MobileScenePackage.mspk");

                // Load it
                MobileScenePackage package = new MobileScenePackage(mobileScenePackagePath);

                await package.LoadAsync();
                
                Scene mapPackageScene = package.Scenes.First();

                scene = mapPackageScene;

                // Change the viewpoint so it doesn't zoom the scene in once it's loaded
                // I don't actually think this works
                scene.InitialViewpoint = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return scene;
        }

        /// <summary>
        /// Loads the building outlines feature layer and sets up the solid fill symbology
        /// </summary>
        public static async Task<FeatureLayer> LoadSolidBuildings(Scene scene)
        {
            FeatureLayer buildingsSolidLayer = new FeatureLayer();

            // Load the buildings layer from the shapefile
            buildingsSolidLayer = (FeatureLayer)scene.OperationalLayers.FirstOrDefault(layer => layer.Name == "SolidBuildings");

            if (buildingsSolidLayer == null)
                throw new Exception("Could not find solid buildings layer");

            // Get the renderer for the buildings layer
            var solidRenderer = (SimpleRenderer) buildingsSolidLayer.Renderer;

            // Set extrusion properties for the solid building outlines layer
            solidRenderer.SceneProperties.ExtrusionMode = ExtrusionMode.BaseHeight;
            solidRenderer.SceneProperties.ExtrusionExpression = "[BHEIGHT]";

            buildingsSolidLayer.Renderer = solidRenderer;

            await buildingsSolidLayer.LoadAsync();

            return buildingsSolidLayer;
        }

        /// <summary>
        /// Loads the building outlines feature layer and sets up the no fill symbology
        /// </summary>
        public static async Task<FeatureLayer> LoadWireframeBuildings(Scene scene)
        {
            FeatureLayer buildingsWireframeLayer = new FeatureLayer();

            buildingsWireframeLayer = (FeatureLayer)scene.OperationalLayers.FirstOrDefault(layer => layer.Name == "WireframeBuildings");

            if (buildingsWireframeLayer == null)
                throw new Exception("Could not find wireframe buildings layer");

            // Set the rendering mode to dynamic to allow for extrusions
            buildingsWireframeLayer.RenderingMode = FeatureRenderingMode.Dynamic;

            // Create a new simple line symbol for the building outlines
            SimpleLineSymbol buildingOutlineLineSymbol = new SimpleLineSymbol(SimpleLineSymbolStyle.Solid, Color.DarkGray, 1);

            // Create a new simple no fill symbol for the wire building outlines
            SimpleFillSymbol buildingOutlineNoFillSymbol = new SimpleFillSymbol(SimpleFillSymbolStyle.Null, Color.Empty, buildingOutlineLineSymbol);

            // Create a new simple renderer for the solid building outlines layers
            SimpleRenderer wireRenderer = new SimpleRenderer(buildingOutlineNoFillSymbol);

            // Set extrusion properties for the solid building outlines layer
            wireRenderer.SceneProperties.ExtrusionMode = ExtrusionMode.BaseHeight;
            wireRenderer.SceneProperties.ExtrusionExpression = "[BHEIGHT]";

            // Apply the renderer to the layer
            buildingsWireframeLayer.Renderer = wireRenderer;

            await buildingsWireframeLayer.LoadAsync();

            return buildingsWireframeLayer;
        }

        /// <summary>
        /// Loads the rooms feature layer
        /// </summary>
        /// <returns></returns>
        public static async Task<FeatureLayer> LoadRooms(Scene scene)
        {
            FeatureLayer rooms = new FeatureLayer();

            rooms = (FeatureLayer)scene.OperationalLayers.FirstOrDefault(layer => layer.Name == "Rooms");

            if (rooms == null)
                throw new Exception("Could not find rooms layer");

            await rooms.LoadAsync();

            return rooms;
        }

        /// <summary>
        /// Load the endpoints feature layer from a shapefile, because for some reason, the mobile scene package layer doesn't work
        /// </summary>
        /// <returns></returns>
        public static async Task<FeatureLayer> LoadEndpoints()
        {
            FeatureLayer endpoints = await LoadFeatureLayerFromShapefile("322732b3b2c64afb9b56c58bcc37f71a", "Endpoints.shp");

            if (endpoints == null)
                throw new Exception("Could not find wireframe buildings layer");

            return endpoints;
        }

        /// <summary>
        /// Load a feature layer from a shapefile path
        /// </summary>
        /// <param name="filename">Name of the shapefile (including the .shp extension)</param>
        /// <param name="layer">Feature layer to load this shapefile into</param>
        /// <returns></returns>
        private static async Task<FeatureLayer> LoadFeatureLayerFromShapefile(string itemId, string filename)
        {
            // Get the full path
            var shapefilePath = await GetData(itemId, filename);

            if (shapefilePath == string.Empty)
                throw new Exception("No shapefile provided for " + filename);

            // Load the shapefile
            ShapefileFeatureTable shapefile = await ShapefileFeatureTable.OpenAsync(shapefilePath);

            // Construct feature layer from shapefile
            var layer = new FeatureLayer(shapefile);

            // Load the layers
            await layer.LoadAsync();

            return layer;
        }
    }
}
