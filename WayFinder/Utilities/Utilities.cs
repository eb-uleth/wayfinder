﻿using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using System;
using System.Linq;
using System.Windows.Shapes;

namespace WayFinder
{
    /// <summary>
    /// Collection of utility methods for the application
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Finds the best location for the camera the encompass the entire envelope.
        /// Loosely based on this comment https://stackoverflow.com/a/32836605
        /// </summary>
        /// <param name="fieldOfView">Field of view of the <see cref="SceneView>"/></param>
        /// <param name="envelope">Envelope trying to be viewed</param>
        /// <returns></returns>
        public static Camera CalculateOptimalCameraPosition(Envelope envelope, double fieldOfView)
        {
            // Get the center of the envelope
            var center = envelope.GetCenter();

            // Get angluar distance to envelope
            var distanceToEnvelopeCenter = ComputeAngluarDistanceFromEnvelope(envelope, fieldOfView);

            // If the width (x-axis length) is larger than the height (y-axis length)
            // We want to orient the camera facing North (0 deg), else, we want to face east (90 deg)
            double heading = envelope.Width > envelope.Height ? 0.0 : 90.0;

            // Compute output camera
            Camera outputCamera = new Camera(center, distanceToEnvelopeCenter, heading, 45, 0);

            return outputCamera;
        }

        /// <summary>
        /// Find the best location for the camera to encompass an entire line segment,
        /// perpendicular to the angle between the two endpoints
        /// </summary>
        /// <param name="lineSegment"></param>
        /// <param name="fieldOfView"></param>
        /// <returns></returns>
        public static Camera CalculateOptimalCameraPosition(LineSegment lineSegment, double fieldOfView)
        {
            // Create an envelope around the line segment
            Envelope envelope = new Envelope(lineSegment.StartPoint, lineSegment.EndPoint);

            // Get the center of the envelope
            var center = envelope.GetCenter();

            // Create a new envelope builder
            EnvelopeBuilder envelopeBuilder = new EnvelopeBuilder(envelope);

            // Expand the envelope to fit within the window
            envelopeBuilder.Expand(1.3);

            // Get angluar distance to envelope 
            var distanceToEnvelope = ComputeAngluarDistanceFromEnvelope(envelopeBuilder.Extent, fieldOfView);

            // Protect against division by 0
            var height = envelope.Height == 0.0f ? 1 : envelope.Height;

            // Get the angle between the two points
            double angleBetweenEndpointsRadians = Math.Tan(envelope.Width / height);

            // Convert to degrees
            double angleBetweenEndpointsDegrees = angleBetweenEndpointsRadians * (180.0 / Math.PI);

            // Add 90 deg and that's the new heading
            double heading = angleBetweenEndpointsDegrees + 90.0;

            Camera outputCamera = new Camera(center, distanceToEnvelope, heading, 45, 0);

            return outputCamera;
        }

        /// <summary>
        /// Returns the optimal distance from an envelope, assuming the point is at a 45 degree vertical angle to the envelope
        /// </summary>
        /// <param name="envelope"></param>
        /// <param name="fieldOfView"></param>
        /// <returns></returns>
        public static double ComputeAngluarDistanceFromEnvelope(Envelope envelope, double fieldOfView)
        {
            // Get the length of the longest side of the envelope
            double envelopeSideLength = Math.Max(envelope.Width, envelope.Height);

            // Compute radius of circle that will encompass envelope
            double encompassRadius = Math.Sqrt(2 * Math.Pow(envelopeSideLength, 2)) * 0.5;

            // Convert field of view to radians
            double fieldOfViewRadians = (Math.PI / 180) * fieldOfView;

            // Compute the distance from camera to envelope
            double verticalDistanceToEnvelope = encompassRadius / Math.Tan(fieldOfViewRadians / 2);

            // Compute angluar distance to envelope, assuming we always look at it from a 45 degree angle (Pi / 4 radians)
            double distanceToEnvelopeCenter = verticalDistanceToEnvelope / Math.Cosh(Math.PI / 4);

            return distanceToEnvelopeCenter;
        }
    }
}
