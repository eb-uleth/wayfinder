﻿using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WayFinder
{
    /// <summary>
    /// Collection of methods to help construc different geometry objects
    /// </summary>
    public static class GeometryHelpers
    {
        /// <summary>
        /// Constructs an envelope that contains all features passed in
        /// </summary>
        /// <param name="features">List of features to contain</param>
        /// <param name="scaleFactor">A factor to expand the envelope by</param>
        /// <param name="computeZ">Flag to determine if you want the elevation to contain z values</param>
        /// <param name="baseSurface">The elevation surface to get the z values from</param>
        /// <returns></returns>
        public static async Task<Envelope> ConstructBoundingEnvelope(IEnumerable<Feature> features, double scaleFactor = 1.3, bool computeZ = false,  Surface baseSurface = null)
        {
            // If there is no features, we cannot compute and envelope
            if (features.Count() == 0)
                return null;

            // Create the envelope builder
            EnvelopeBuilder envelopeBuilder = new EnvelopeBuilder(features.First().Geometry.SpatialReference);

            // Iterate through input features and add their extent to the envelope builder
            features.ToList().ForEach(feature => envelopeBuilder.UnionOf(feature.Geometry.Extent));

            // Expand the envelope by the scale factor
            envelopeBuilder.Expand(scaleFactor);

            // Set the vertical coordinates for the envelope
            if (computeZ)
            {
                if (baseSurface == null)
                    throw new Exception("Must provide base surface to compute elevation of envelope");

                var centerPointElevation = await baseSurface.GetElevationAsync(envelopeBuilder.Center);
                envelopeBuilder.SetZ(centerPointElevation, centerPointElevation);
            }

            return envelopeBuilder.Extent;
        }

        /// <summary>
        /// Constructs an envelope that contains all geometries passed in
        /// </summary>
        /// <param name="geometries">List of geometries</param>
        /// <param name="scaleFactor">A factor to expand the envelope by</param>
        /// <returns></returns>
        public static Envelope ConstructBoundingEnvelope(IEnumerable<Geometry> geometries, double scaleFactor = 1.3)
        {
            if (geometries.Count() == 0)
                return null;

            // Create the envelope builder
            EnvelopeBuilder envelopeBuilder = new EnvelopeBuilder(geometries.First().Extent);

            // Iterate through geometries and add their extent to the envelope builder
            geometries.ToList().ForEach(geometry => envelopeBuilder.UnionOf(geometry.Extent));

            // Expand the envelope by the scale factor
            envelopeBuilder.Expand(scaleFactor);

            return envelopeBuilder.Extent;
        }

    }
}
