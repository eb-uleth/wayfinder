﻿using Esri.ArcGISRuntime.Data;

namespace WayFinder
{
    /// <summary>
    /// Collection of Methods to help build queries
    /// </summary>
    public static class QueryHelpers
    {
        /// <summary>
        /// Builds a <see cref="QueryParameters"/> object to select features based on the BNAME field
        /// This method assumcs any single quotations in the <paramref name="buildingName"/> parameter have been escaped
        /// </summary>
        /// <param name="buildingName">Name of the building to query for</param>
        /// <returns></returns>
        public static QueryParameters BuildBuildingNameQueryParameters(string buildingName, string additionalQueries = "")
        {
            // Set the query parameter to select the building with the same name
            var query = new QueryParameters()
            {
                WhereClause = string.Format("BNAME = '{0}' {1}", buildingName, additionalQueries)
            };

            return query;
        }
    }
}
